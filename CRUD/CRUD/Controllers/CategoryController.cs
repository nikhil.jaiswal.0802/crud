﻿using CRUD.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;
using System.Data.SqlClient;

namespace CRUD.Controllers
{

    public class CategoryController : Controller
    {



        Context db = new Context();


        //------------------ Category List ------------------------------
        //                   ==============

        [Authorize]
        public async Task<ActionResult> Index(int? page)
        {
            var a =  await Task.Run(() => db.categories.ToList().ToPagedList(page ??1,3));
                     
            return View(a); 
        }


        //------------------ Add New Category ------------------------------
        //                    ==============

        [Authorize(Roles = "Admin")]
        public ActionResult AddCategory()
        {
            var C = new Category();
            C.Activate = true;
            return View(C);
        }



        //------------------ Add New Category(Post Method) ----------------------------
        //                   =============================

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> AddCategory(Category obj)
        {
            if(obj == null)
            {
                ViewData["Message"] = "<script>alert('Please fill details')</script>";
            }
            else
            {             
                db.categories.Add(obj);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            
            return View();
        }


        //------------------ Delete Category ------------------------------
        //                   ===============

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteCategory(int id,int? page)
        {
            var row = db.categories.Where(model => model.CategoryId == id).FirstOrDefault();

            var productRow = db.products.Where(model => model.CategoryRefId == id).ToList();

            if (row != null | productRow.Count > 0)
            {
                db.Entry(row).State = EntityState.Deleted;
                await db.SaveChangesAsync();
            }

            return RedirectToAction("Index",new { page = page });

        }


        //------------------ Edit Category ------------------------------
        //                   =============

        [Authorize(Roles = "Admin")]
        public ActionResult EditCategory(int id)
        {
            var row = db.categories.Where(model => model.CategoryId == id).FirstOrDefault();
            return View(row);
        }


        //------------------ Edit Category(Post Method) --------------------
        //                   ==========================

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> EditCategory(Category cat)
        {

            db.Entry(cat).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


        //------------------ Activate & Deavtivate ---------------------------
        //                   =====================

        public async Task<ActionResult> Activate_Deactivate(int id)
        {
           
            var results = (from p in db.categories
                                    where p.CategoryId == id
                                    select p).ToList();

            foreach (var p in results)
            {
                if(p.Activate == true)
                {
                    p.Activate = false;
                }
                else
                {
                    p.Activate = true;
                }
                
            }
            
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

       

    }



}