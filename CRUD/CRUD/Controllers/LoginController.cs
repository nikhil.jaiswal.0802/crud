﻿using CRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CRUD.Controllers
{
    
    public class LoginController : Controller
    {

        Context db = new Context();



        // --------------- LOGIN ----------------------------

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(User u, string ReturnUrl)
        {
            var row = db.users.Where(model => model.username == u.username & model.password == u.password).FirstOrDefault();

            if (row == null)
            {
                ModelState.Clear();
                ViewData["Error"] = "<script>alert('Invalid Credentials')</script>";
                return View();
            }
            else
            {
                var token = JWT_Authentication_Manager.Authenticate(u.username);
                Response.Cookies.Set(new HttpCookie("token", token));
                Session["username"] = u.username.ToString();
                //FormsAuthentication.SetAuthCookie(u.username, false);
                if (ReturnUrl != null)
                {                    
                    return Redirect(ReturnUrl);
                }
                else
                {                   
                    return RedirectToAction("Index", "Category");
                }

            }

        }


        // --------------- SIGN UP ----------------------------
        public ActionResult SignUp()
        {

            return View();
        }



        [HttpPost]
        public ActionResult SignUp(User u)
        {
            if (u.username != null & u.password != null)
            {
                db.users.Add(u);
                db.SaveChanges();
                return RedirectToAction("Index","Login");
            }
            else
            {
                ViewData["Message"] = "<script>alert('Please fill details')</script>";
                return View();
            }
        }

        // --------------- LOG OUT ----------------------------

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session["username"] = null;
            Response.Cookies["token"].Value = null;
            return RedirectToAction("Index", "Login");
        }
    }
}