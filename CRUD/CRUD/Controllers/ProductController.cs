﻿using CRUD.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CRUD.Controllers
{
    public class ProductController : Controller
    {
        Context db = new Context();



        //------------------ Product List ------------------------------
        //                   ==============
        [AllowAnonymous]
        public async Task<ActionResult> Product(int id, int? page)
        {
            Session["id"] = id;
            ViewData["Message"] = "No Products";

            var productList = await Task.Run(() =>
            db.products.Where(model => model.CategoryRefId == id).ToList().ToPagedList(page ?? 1, 3));

            return View(productList);
        }



        //------------------ Add Product ------------------------------
        //                   ===========

        [Authorize(Roles = "Admin")]
        public ActionResult AddProduct(int id)
        {

            var p = new Product();

            string username = Session["username"].ToString().ToLower();
            var result = (from c in db.users
                          where c.username == username
                          select c).ToList();

            foreach (var item in result)
            {
                p.UserRefId = item.UserId;
            }

            p.CategoryRefId = id;

            return View(p);
        }

        //------------------ Add Product(Post Method) ------------------------------
        //                   =======================

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> AddProduct(Product p)
        {
            db.products.Add(p);
            await db.SaveChangesAsync();
            return RedirectToAction("Product",new { id = Session["id"]});

        }


        //------------------ Edit Product ------------------------------
        //                   ============

        [Authorize(Roles = "Admin")]
        public ActionResult EditProduct(int id)
        {
            var row = db.products.Where(model => model.ProductId == id).FirstOrDefault();
            return View(row);
        }


        //------------------ Edit Product(Post Method) ------------------------------
        //                   =======================
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> EditProduct(Product p)
        {

            db.Entry(p).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Product", new { id = Session["id"] });

        }


        //------------------ Delete Product ------------------------------
        //                   ==============
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteProduct(int id)
        {
            var row = db.products.Where(model => model.ProductId == id).FirstOrDefault();

            if (row != null)
            {
                db.Entry(row).State = EntityState.Deleted;
                await db.SaveChangesAsync();
            }

            return RedirectToAction("Product", new { id = Session["id"] });

        }
    }
}