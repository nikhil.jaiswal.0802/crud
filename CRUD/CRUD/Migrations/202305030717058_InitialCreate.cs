﻿namespace CRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        Activate = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        ProductPrice = c.Single(nullable: false),
                        CategoryRefId = c.Int(),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Category", t => t.CategoryRefId)
                .Index(t => t.CategoryRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "CategoryRefId", "dbo.Category");
            DropIndex("dbo.Product", new[] { "CategoryRefId" });
            DropTable("dbo.Product");
            DropTable("dbo.Category");
        }
    }
}
