﻿namespace CRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdBy : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        username = c.String(),
                        password = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            AddColumn("dbo.Product", "UserRefId", c => c.Int());
            CreateIndex("dbo.Product", "UserRefId");
            AddForeignKey("dbo.Product", "UserRefId", "dbo.Users", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "UserRefId", "dbo.Users");
            DropIndex("dbo.Product", new[] { "UserRefId" });
            DropColumn("dbo.Product", "UserRefId");
            DropTable("dbo.Users");
        }
    }
}
