﻿namespace CRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Reports");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        ProductName = c.String(),
                        ProductPrice = c.Single(nullable: false),
                        CreadedBy = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
    }
}
