﻿namespace CRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class key : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Roles", "UserRefId", "dbo.Users");
            DropIndex("dbo.Roles", new[] { "UserRefId" });
            AddColumn("dbo.Users", "Roles_id", c => c.Int());
            CreateIndex("dbo.Users", "Roles_id");
            AddForeignKey("dbo.Users", "Roles_id", "dbo.Roles", "id");
            DropColumn("dbo.Roles", "UserRefId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Roles", "UserRefId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Users", "Roles_id", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "Roles_id" });
            DropColumn("dbo.Users", "Roles_id");
            CreateIndex("dbo.Roles", "UserRefId");
            AddForeignKey("dbo.Roles", "UserRefId", "dbo.Users", "UserId", cascadeDelete: true);
        }
    }
}
