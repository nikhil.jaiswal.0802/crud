﻿namespace CRUD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class keyw : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "Roles_id", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "Roles_id" });
            RenameColumn(table: "dbo.Users", name: "Roles_id", newName: "RoleRefId");
            AlterColumn("dbo.Users", "RoleRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.Users", "RoleRefId");
            AddForeignKey("dbo.Users", "RoleRefId", "dbo.Roles", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "RoleRefId", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "RoleRefId" });
            AlterColumn("dbo.Users", "RoleRefId", c => c.Int());
            RenameColumn(table: "dbo.Users", name: "RoleRefId", newName: "Roles_id");
            CreateIndex("dbo.Users", "Roles_id");
            AddForeignKey("dbo.Users", "Roles_id", "dbo.Roles", "id");
        }
    }
}
