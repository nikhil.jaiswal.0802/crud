﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CRUD.Models
{
    public class CategoryList
    {

        [Key]
        public Int64 CategoryId { get; set; }

        [DisplayName("Category Name")]
        public string CategoryName { get; set; }

        public bool Activate { get; set; }


    }
}