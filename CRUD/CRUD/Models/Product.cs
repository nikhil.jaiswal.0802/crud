﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRUD.Models
{
    [Table("Product")]
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        [DisplayName("Price")]
        public float ProductPrice { get; set; }


        [ForeignKey("Category")]
        public int? CategoryRefId { get; set; }

        
        public  Category Category { get; set; }

        [ForeignKey("User")]
        public int? UserRefId { get; set; }
        public User User { get; set; }
    }
}