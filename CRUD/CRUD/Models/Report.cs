﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CRUD.Models
{
    public class Report
    {
        [DisplayName("Sr no.")]
        public int id { get; set; }

        public string Category { get; set; }

        
        public string Product { get; set; }

        
        public float Price { get; set; }
        
        [DisplayName("Created By")]
        public string CreatedBy { get; set; }
    }
}