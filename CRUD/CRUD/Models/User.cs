﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CRUD.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        public string username { get; set; }

        [DataType(DataType.Password)]
        public string password { get; set; }

        public Roles Roles { get; set; }

        [ForeignKey("Roles")]
        public int RoleRefId { get; set; } = 2;
    }
}