﻿using FormAutthentication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FormAutthentication.Controllers
{
    public class HomeController : Controller
    {


        UserContext db = new UserContext();
      
        
        public ActionResult Index()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Index(User u ,string ReturnUrl)
        {
            var row = db.users.Where(model => model.username == u.username & model.password == u.password).FirstOrDefault();

            if (row == null)
            {
                ModelState.Clear();
                ViewData["Error"] = "<script>alert('Invalid Credentials')</script>";
                return View();
            }
            else
            {
                FormsAuthentication.SetAuthCookie(u.username,false);
                if(ReturnUrl != null)
                {
                    Session["username"] = u.username.ToString();
                    return Redirect(ReturnUrl);
                    
                }
                else
                {
                    Session["username"] = u.username.ToString();
                    return RedirectToAction("Contact", "Home");
                }
                
            }
        }

        public ActionResult SignUp()
        {

            return View();
        }

        [HttpPost]
        public ActionResult SignUp(User u)
        {
            if(u.username!= null & u.password !=null)
            {
                db.users.Add(u);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["Message"] = "<script>alert('Please fill details')</script>";
                return View();
            }
            
        }

        public ActionResult About()
        {
            return View();

        }

        [Authorize]
        public ActionResult contact()
        {
            return View();
        }

    }
}