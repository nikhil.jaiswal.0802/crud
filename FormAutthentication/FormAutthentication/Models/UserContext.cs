﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FormAutthentication.Models
{
    public class UserContext :DbContext
    {

        public UserContext() : base ("name=Connection")
        {

        }

        public DbSet<User> users { get; set; }
    }
}