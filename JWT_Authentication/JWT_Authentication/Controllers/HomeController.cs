﻿using System;
using System.Collections.Generic;
using System.Linq;
using JWT_Authentication.Models;
using System.Web;
using System.Web.Mvc;

namespace JWT_Authentication.Controllers
{
    public class HomeController : Controller
    {


        Context db = new Context();

        [Authorize]
        public ActionResult Index()
        {
            return View(db.products.ToList()) ;
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(Product p)
        {
            db.products.Add(p);
            db.SaveChanges();
            return View();
        }




    }
}