﻿using JWT_Authentication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JWT_Authentication.Controllers
{
    public class LoginController : Controller
    {


        Context db = new Context();

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(User u)
        {

            var row = db.users.Where(model => model.username == u.username & model.password == u.password);
            

            if (row == null)
            {
                ModelState.Clear();
                ViewData["Error"] = "<script>alert('Invalid Credentials')</script>";
                return View();
            }              
            else
            {
                var token = JWT_Authenticattion_Manager.Authenticate(u.username);
                Response.Cookies.Set(new HttpCookie("token", token));
                return RedirectToAction("Index", "Home");
            }               

        }


        [AllowAnonymous]
        public ActionResult SignUp()
        {

            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult SignUp(User u)
        {
            if (u.username != null & u.password != null)
            {
                db.users.Add(u);
                db.SaveChanges();
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewData["Message"] = "<script>alert('Please fill details')</script>";
                return View();
            }
        }

        public ActionResult LogOut()
        {
          
            Session["username"] = null;
            return RedirectToAction("Index", "Login");
        }
    }
}