﻿using JWT_Authentication.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Web;
using System.Security.Claims;
using System.Threading;

namespace JWT_Authentication
{
    public class JWT_Authenticattion_Manager
    {
        public const int JWT_TOKEN_VALIDITY = 1;
        public const string JWT_SECURITY_KEY = "QAWSEDRFTGYHUJIK";

        public static string Authenticate(string user)
        {         

            var tokenExpiryTimeStamp = DateTime.Now.AddMinutes(JWT_TOKEN_VALIDITY);
            var tokenKey = Encoding.ASCII.GetBytes(JWT_SECURITY_KEY);
            var securityTokenDiscriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(new List<Claim>
                {
                    new Claim("username", user),
                }),
                Issuer = "https://localhost:44386/",
                Audience = "https://localhost:44386/",
                Expires = tokenExpiryTimeStamp,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)

            };

            var securityToken = new JwtSecurityTokenHandler().CreateToken(securityTokenDiscriptor);
            var token = new JwtSecurityTokenHandler().WriteToken(securityToken);

            return token;
            //{
            // token = token,
            // username = user,
            // expires_in =(int) tokenExpiryTimeStamp.Subtract(DateTime.Now).TotalSeconds
            // };
        }

        public static ClaimsPrincipal ValidatejwtToken(string token)
        {

            var h = new JwtSecurityTokenHandler();
            h.ValidateToken(token, new TokenValidationParameters()
            {

                RequireExpirationTime = true,
                ValidAlgorithms = new[] { SecurityAlgorithms.HmacSha256 },
                ValidateAudience = false,
                ValidateIssuer = false,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JWT_SECURITY_KEY)),
                ValidateIssuerSigningKey = true,
               // ValidateLifetime = false,

            }, out var securityToken); ;
            var jwt = securityToken as JwtSecurityToken;
            var id = new ClaimsIdentity(jwt.Claims,"jwt","username", ClaimTypes.Role);
            return new ClaimsPrincipal(id);
        }

        public static void AuthenticationRequest(string token)
        {
            var principal = ValidatejwtToken(token);
            HttpContext.Current.User = principal;
            Thread.CurrentPrincipal = principal;
        }
    }


   
}