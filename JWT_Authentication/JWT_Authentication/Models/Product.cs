﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JWT_Authentication.Models
{
    public class Product
    {
        [DisplayName("SrNo")]
        [Key]
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }


    }
}